
export namespace IBenchmark {

	export type FuncArgCallback = (i: number, funcName: string, func: Function) => any[];
	
	export type FuncAggregator = (i: number, funcName: string, result: any, errorMsg?: string) => string;

	export interface FuncOptions {
		/** 
		 * String to add after func name. Used to mark same functions to put them into different groups.
		 */
		tag?: string;
		/** 
		 * Arguments for function call or callback to determine args depends on some params
		 */
		args?: any[] | FuncArgCallback;
		/** 
		 * Maximum call duration, after this function will stop and call-duration will be -1 
		 */
		timeoutMs?: number;
		/**
		 * Function returning name of group to aggregate call info
		 */
		aggregator?: FuncAggregator;
	}

	export interface FuncOptionsFull extends FuncOptions {
		/**
		 * Is function will be called outside and durations will be come from outside
		 */
		isExternal: boolean;
	}

	export type CallStatsMap = Record<string, CallStats>;

	export interface CallStatsBase {
		/** duration or void if error */
		durations: (number | void)[];
		/** sum of all call durations */
		totalDuration: number;
		/** amount of errors */
		errors: number;
	}

	export interface AggregatedCallStats extends CallStatsBase {
		/** indexes of aggregated calls */
		durationsI: number[];
	}

	export interface CallStats extends CallStatsBase {
		aggregated: Record<string, AggregatedCallStats>;
	}

	export interface Analytics {
		fastestFunc: string;
		items: Record<string, ItemAnalytics>;
	}

	export interface ItemAnalyticsBase {
		diffs: (number | void)[];
		diffsPercents: (number | void)[];
		totalDurationDiffPercent: number;
		totalDurationDiff: number;
		minTime: number | void;
		avgTime: number;
	}

	export type AggregatedItemAnalytics = Union<AggregatedCallStats, ItemAnalyticsBase>;

	export interface ItemAnalytics extends CallStatsBase, ItemAnalyticsBase {
		aggregated: Record<string, AggregatedItemAnalytics>;
	}
}
