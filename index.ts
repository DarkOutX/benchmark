
import Benchmark from './Benchmark';

async function longerFunc() {
	return new Promise(res => setTimeout(res, 25));
}

async function longFunc() {
	return new Promise(res => setTimeout(res, 15));
}

async function longFuncTimed(sleep = 10) {
	return new Promise(res => setTimeout(res, sleep));
}

function iter() {
	let counter = 0;

	for (let i = 0; i < 1000000; i++) {
		counter++;
	}
}

async function main_() {
	const bench = new Benchmark();

	bench.addFunc(iter);
	bench.addFunc(longFuncTimed, {
		args: (i: number) => {
			if (i < 5) {
				return [10];
			}

			if (i < 10) {
				return [30];
			}

			return [75];
		},
		aggregator: (i) => {
			if (i < 5) {
				return '10ms';
			}

			if (i < 10) {
				return '30ms';
			}

			return '75ms';	
		}
	});
	bench.addFunc(longFunc);

	await bench.start(15);

	console.log(bench.analyze().items['longFuncTimed'].aggregated);
}

async function main() {
	const bench = new Benchmark();

	for (let i = 0; i < 10; i++) {
		const key = bench.timeStart(longFunc);

		await longFunc();

		bench.timeEnd(key);		
	}

	bench.addFunc(longerFunc);

	await bench.start();

	const analytics = bench.analyze();

	Object.keys(analytics).forEach(key => {
		console.log(key, analytics[key]);
	});
}

main();