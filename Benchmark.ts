
import type {IBenchmark} from './types/types';

let localCounter = 1;

export default class Benchmark {

	private _funcs: Function[] = [];
	private _funcsMap: Record<string, Function> = {};
	private _funcsOptions: Record<string, IBenchmark.FuncOptionsFull> = {};
	private _stats: IBenchmark.CallStatsMap = {};
	private _iterationsAmount: number = 0;
	private _externalMode = false;
	private _runningFuncs: Record<string, {start: number, funcName: string}> = {};

	get funcs() {
		return this._funcs;
	}

	get funcsMap() {
		return this._funcsMap;
	}

	get funcsOptions() {
		return this._funcsOptions;
	}

	get stats() {
		return this._stats;
	}

	timeStart(func: Function, options: IBenchmark.FuncOptions = {}, now = Date.now()) {
		const funcName = this._addFunc(func, {...options, isExternal: true});
		const curKey = `${funcName}_${++localCounter}`;

		this._runningFuncs[curKey] = {
			start: now,
			funcName,
		};

		return curKey;
	}

	timeEnd(key: string, now = Date.now(), isError = false) {
		const runStats = this._runningFuncs[key];

		if (!runStats) {
			console.error(`[Benchmark#endTime] No func by key ${key}`);

			return;
		}

		const {
			start,
			funcName,
		} = runStats;
		const funcStats = this._stats[funcName] || _statsBlockContructor();

		if (start > now) {
			console.error(`[Benchmark#endTime] Wrong end(${now}) or start(${start}) time`);

			return;
		}

		if (isError) {
			funcStats.durations.push(void 0);
			funcStats.errors++;
		}
		else {
			const duration = now - start;

			funcStats.durations.push(duration);
			funcStats.totalDuration += duration;
		}

		if (!this._stats[funcName]) {
			this._stats[funcName] = funcStats;
		}

		const curAmount = this._iterationsAmount;
		const newAmount = funcStats.durations.length;

		if (curAmount < newAmount) {
			this._iterationsAmount = newAmount;
		}

		this._externalMode = true;

		delete this._runningFuncs[key];
	}

	addFunc(func: Function, options: IBenchmark.FuncOptions = {}) {
		this._addFunc(func, {...options, isExternal: false});
	}

	private _addFunc(func: Function, options: IBenchmark.FuncOptionsFull) {
		const name = Benchmark.createName(func.name, options.tag);

		this._funcs.push(func);
		this._funcsMap[name] = func;
		this._funcsOptions[name] = options;

		return name;
	}

	reset() {
		this._iterationsAmount = 0;
		this._stats = {};
	}

	async start(amount = 1) {
		if (!this._externalMode) {
			this.reset();
			this._iterationsAmount = amount;
		}

		const funcNames = Object.keys(this._funcsMap);

		for (let i = 0; i < funcNames.length; i++) {
			const funcName = funcNames[i];
			const func = this._funcsMap[funcName];
			const options = this._funcsOptions[funcName];
			let curAmount = amount;

			if (options.isExternal) {
				continue;
			}


			if (this._externalMode) {	
				const alreadyCalledAmount = this._stats[funcName]?.durations?.length || 0;
	
				curAmount = this._iterationsAmount - alreadyCalledAmount;
			}

			await this.wrapMult(func, curAmount, options);
		}

		return this._stats;
	}

	analyze(): IBenchmark.Analytics {
		const amount = this._iterationsAmount;
		const stats = this._stats;
		const funcNames = Object.keys(stats);

		let bestDuration = Infinity;
		let bestFuncName = '';
		let bestDurations: (number | void)[] = [];

		if (!amount) {
			throw new Error('[Error#analyze] No iterations');
		}

		// Finding best func
		for (let i = 0; i < funcNames.length; i++) {
			const funcName = funcNames[i];
			const {
				durations,
				totalDuration,
			} = stats[funcName];

			if (totalDuration < bestDuration) {
				bestDuration = totalDuration;
				bestFuncName = funcName;
				bestDurations = durations;
			}
		}

		const analytics: IBenchmark.Analytics = {
			fastestFunc: bestFuncName,
			items: {},
		};

		// Analyzing diffs
		for (let i = 0; i < funcNames.length; i++) {
			const funcName = funcNames[i];
			const {
				totalDuration,
				durations,
				errors,
				aggregated,
			} = stats[funcName];
			const [
				diffs, 
				diffsPercents,
				minTime,
			] = _compareDurations(bestDurations, durations);
			const aggregatedAnalytics: Record<string, IBenchmark.AggregatedItemAnalytics> = {};
			const groupNames = Object.keys(aggregated);

			groupNames.forEach((name) => {
				const {
					durationsI,
					durations,
					totalDuration,
				} = aggregated[name];
				let bestDurationForGroup = 0;
				const bestGroupDurations = durationsI.map((i, localI) => {
					const bestDuration = bestDurations[i];

					if (bestDuration !== void 0) {
						bestDurationForGroup += bestDuration;
					}

					return bestDuration;
				});
				const [
					diffs, 
					diffsPercents,
					minTime,
				] = _compareDurations(bestGroupDurations, durations);

				aggregatedAnalytics[name] = {
					...aggregated[name],
					avgTime: totalDuration / Math.abs(durations.length - errors),
					minTime,
					diffs,
					diffsPercents,
					totalDurationDiff: totalDuration - bestDurationForGroup,
					totalDurationDiffPercent: _countDiffPercent(bestDurationForGroup, totalDuration),
				}				
			});

			const itemAnalytics: IBenchmark.ItemAnalytics = {
				...stats[funcName],
				aggregated: aggregatedAnalytics,
				avgTime: totalDuration / Math.abs(durations.length - errors),
				minTime,
				diffs,
				diffsPercents,
				totalDurationDiff: totalDuration - bestDuration,
				totalDurationDiffPercent: _countDiffPercent(bestDuration, totalDuration),
			};

			analytics.items[funcName] = itemAnalytics;
		}

		return analytics;
	}

	async wrapMult(func: Function, amount: number, options?: IBenchmark.FuncOptions) {
		const [funcName, statsBlock] = await Benchmark.wrapMult(func, amount, options);

		this._stats[funcName] = statsBlock;

		return [funcName, statsBlock];
	}

	static createName(name: string, tag?: string) {
		return tag ? `${name}#${tag}` : name;
	}

	static async wrap(func: Function, options?: IBenchmark.FuncOptions, i = 0): Promise<[
		funcName: string,
		duration: number | void,
		result?: any,
		errorMsg?: string,
	]> {
		const {
			tag,
			args: argsOrCallback,
			timeoutMs,
		} = options || {};
		const funcName = Benchmark.createName(func.name, options?.tag);
		const args = typeof argsOrCallback === 'function' ? argsOrCallback(i, func.name, func) : argsOrCallback;
		const [timeoutPromise, timeout] = timeoutMs ? _createTimeout(timeoutMs) : [void 0, void 0];
		const time = Date.now();
		let result = void 0;
		
		try {
			if (timeoutPromise) {
				await Promise.race([
					func.apply(void 0, args),
					timeoutPromise,
				]).then(([res]) => result = res);
			}
			else {
				result = await func.apply(void 0, args);
			}
		}
		catch (err) {
			if (timeout !== void 0) {
				clearTimeout(timeout);
			}

			const errorMsg: string = (err as any).message ? (err as any).message : 'unknown';

			return [funcName, void 0, result, errorMsg];
		}

		const totalTime = Date.now() - time;

		if (timeout !== void 0) {
			clearTimeout(timeout);
		}

		return [funcName, totalTime, result, void 0];
	}

	static async wrapMult(func: Function, amount: number, options?: IBenchmark.FuncOptions): Promise<[
		funcName: string,
		statsBlock: IBenchmark.CallStats,
	]> {
		const funcStats = _statsBlockContructor();
		const {
			aggregator,
		} = options || {};
		const isHasAggrFunc = (typeof aggregator === 'function');
		let returnFuncName = '';
			
		for (let i = 0; i < amount; i++) {
			const [funcName, duration, result, errorMsg] = await Benchmark.wrap(func, options, i);

			if (!returnFuncName) {
				returnFuncName = funcName;
			}

			if (errorMsg !== void 0) {
				funcStats.durations.push(void 0);
				funcStats.errors++;
			}
			else {
				funcStats.durations.push(duration);
				funcStats.totalDuration += duration!;
			}

			if (isHasAggrFunc) {
				const groupName = aggregator(i, funcName, void 0, errorMsg);

				if (!funcStats.aggregated[groupName]) {
					funcStats.aggregated[groupName] = _statsBlockContructor(true);
				}

				const aggregatedStats = funcStats.aggregated[groupName];

				aggregatedStats.durations.push(duration);
				aggregatedStats.durationsI.push(i);

				if (errorMsg !== void 0) {
					aggregatedStats.errors++;
				}
				else {
					aggregatedStats.totalDuration += duration!;
				}
			}
		}

		return [returnFuncName, funcStats];
	}
}

function _compareDurations(best: (void | number)[], comparing: (void | number)[]): [
	diffs: (void | number)[], 
	percents: (void | number)[],
	minDur: void | number,
] {
	const diffs: (void | number)[] = [];
	const diffsPercents: (void | number)[] = [];
	let minDur: number | void = comparing[0];

	if (best.length !== comparing.length) {
		console.error('[_compareDurations] Wrong durations amount');

		return [[], [], void 0];
	}

	for (let i = 0; i < best.length; i++) {
		const bestDur = best[i];
		const dur = comparing[i];

		if ((dur && dur < minDur) || (minDur === void 0)) {
			minDur = dur;
		}

		if (dur === void 0 || bestDur === void 0) {
			diffsPercents.push(void 0);
			diffs.push(void 0);

			continue;
		}

		const diffPercent = _countDiffPercent(bestDur, dur);

		diffs.push(dur - bestDur);
		diffsPercents.push(diffPercent);
	}

	return [diffs, diffsPercents, minDur];
}

function _statsBlockContructor(isAggregated?: false): IBenchmark.CallStats
function _statsBlockContructor(isAggregated: true): IBenchmark.AggregatedCallStats
function _statsBlockContructor(isAggregated?: boolean): IBenchmark.CallStats | IBenchmark.AggregatedCallStats {
	if (isAggregated) {
		return {
			durationsI: [],
			durations: [],
			totalDuration: 0,
			errors: 0,
		} as IBenchmark.AggregatedCallStats;
	}
	else {
		return {
			durations: [],
			totalDuration: 0,
			errors: 0,
			aggregated: {},
		} as IBenchmark.CallStats;
	}
}

function _createTimeout(ms: number) {
	let timeout;
	let resolver;

	const promise = new Promise((resolve, reject) => {
		timeout = setTimeout(() => {reject('Timeout')}, ms);
		resolver = resolve;
	});

	return [promise, timeout];
}

function _countDiffPercent(origin: number, comparing: number, pads = 2) {
	const divider = Math.pow(10, 2 + pads);
	const backDivider = Math.pow(10, pads);

	if (origin === 0) {
		return Infinity;
	}

	return Math.floor(((comparing / origin) - 1) * divider) / backDivider;
}